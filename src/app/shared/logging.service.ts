import { MY_INJECTION_TOKEN } from './injection.tokens';
import { Inject } from '@angular/core';
import { ServiceConfig } from './injection-token.types';

export class LoggingService {

  instanceId = Math.floor(Math.random() * 10000) + 1000;

  constructor(@Inject(MY_INJECTION_TOKEN) private config: ServiceConfig) {
    console.log('Service Constructor: ' + config.defaultMessage);
  }

  log(msg: string) {
    console.log(`Service-${this.instanceId} - ${msg}`);
  }
}
