import { InjectionToken } from '@angular/core';
import { ServiceConfig } from './injection-token.types';

export const MY_INJECTION_TOKEN = new InjectionToken<ServiceConfig>('MyToken');
