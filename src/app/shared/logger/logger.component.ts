import { Component, Input, OnInit } from '@angular/core';
import { LoggingService } from '../logging.service';

@Component({
  selector: 'app-logger',
  templateUrl: './logger.component.html',
  styleUrls: ['./logger.component.sass']
})
export class LoggerComponent implements OnInit {

  @Input() message: string;

  constructor(private loggingService: LoggingService) { }

  ngOnInit() {
  }

  log() {
    this.loggingService.log(this.message);
  }

}
