import { Component, OnInit } from '@angular/core';
import { LoggingService } from '../../shared/logging.service';

@Component({
  selector: 'app-di2',
  templateUrl: './di2.component.html',
  styleUrls: ['./di2.component.sass']
})
export class Di2Component implements OnInit {

  constructor(private loggingService: LoggingService) { }

  ngOnInit() {
  }

  log() {
    this.loggingService.log('DI2 Component!');
  }

}
