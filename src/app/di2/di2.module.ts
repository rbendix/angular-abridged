import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Di2Component } from './di2/di2.component';
import { MY_INJECTION_TOKEN } from '../shared/injection.tokens';
import { LoggingService } from '../shared/logging.service';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: '', component: Di2Component }
];

@NgModule({
  declarations: [Di2Component],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [
    { provide: MY_INJECTION_TOKEN, useValue: { defaultMessage: 'DI2 Module Message'} },
    LoggingService
  ]
})
export class Di2Module { }
