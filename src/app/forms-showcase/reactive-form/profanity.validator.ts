import { AbstractControl } from '@angular/forms';

const profanities = [
    'freibierlätchn',
    'brunzkachel'
];

export function profanity(ctrl: AbstractControl) {
  return ctrl.value && profanities.some(p => ctrl.value.toLowerCase().includes(p)) ?
      { profanity: true } :
      null;
}
