import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { profanity } from './profanity.validator';
import { NameInUseService } from './name-in-use.service';
import { nameInUse } from './name-in-use.validator';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {

  myForm: FormGroup;
  requiredMaxLength: Validators[] = [Validators.required, Validators.maxLength(10)];
  skillName = '';
  namePending: Observable<boolean>;

  constructor(private formBuilder: FormBuilder, private nameInUseService: NameInUseService) {
  }

  get skills(): FormArray {
    return this.myForm.get('skills') as FormArray;
  }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      firstName: [null, this.requiredMaxLength, [nameInUse(this.nameInUseService)]],
      lastName: [null, this.requiredMaxLength],
      email: [null, [Validators.required, Validators.email, profanity]],
      age: [null, [Validators.required, Validators.min(0), Validators.max(100)]],
      skills: this.formBuilder.array([], [Validators.required])
    });

    this.namePending = this.myForm.get('firstName').statusChanges.pipe(map(status => status === 'PENDING'));

    this.myForm.get('firstName').valueChanges.subscribe(console.log);
  }

  addSkill() {
    const skills: FormArray = this.myForm.get('skills') as FormArray;
    skills.push(this.formBuilder.group({
      name: this.skillName,
      level: [0, Validators.min(1)]
    }));

    this.skillName = '';
  }

  submit() {
    console.log(this.myForm.get('firstName').value);
    console.log(this.myForm.get('lastName').value);
    console.log(this.myForm.get('age').value);
    console.log(this.myForm);
  }

  deleteSkill(index: number) {
    const skills: FormArray = this.myForm.get('skills') as FormArray;
    skills.removeAt(index);
  }
}
