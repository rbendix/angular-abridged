import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class NameInUseService {
  isNameInUse(name: string): Observable<boolean> {
    if (name === 'Hans') {
      return of(true).pipe(delay(800));
    }
    return of(false);
  }
}
