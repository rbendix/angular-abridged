import { AbstractControl } from '@angular/forms';
import { NameInUseService } from './name-in-use.service';
import { map } from 'rxjs/operators';

export function nameInUse(service: NameInUseService) {
  return (ctrl: AbstractControl) => {
    if (ctrl.value) {
      return service.isNameInUse(ctrl.value).pipe(
          map(inUse => inUse ? {nameInUse: true} : null));
    }

    return null;
  };
}
