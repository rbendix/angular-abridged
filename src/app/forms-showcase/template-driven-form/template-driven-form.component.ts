import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent implements OnInit {

  @ViewChild('myForm') myForm: FormGroup;

  firstName: string;
  lastName: string;
  age: number;

  constructor() { }

  ngOnInit() {
  }

  submit() {
    console.log(this.firstName);
    console.log(this.lastName);
    console.log(this.age);
    console.log(this.myForm);
  }

  handleChange(value: string) {
    console.log(value);
  }
}
