import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ObservablesComponent } from './observables.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { SubjectComponent } from './subject/subject.component';

const routes: Routes = [
    {
      path: '',
      component: SubjectComponent
    }
];

@NgModule({
  declarations: [ObservablesComponent, SubjectComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class ObservablesModule { }
