import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-observables',
  templateUrl: './observables.component.html',
  styleUrls: ['./observables.component.sass']
})
export class ObservablesComponent implements OnInit {

  formControl: FormControl = new FormControl();

  constructor() { }

  ngOnInit() {
    this.formControl.valueChanges.subscribe(console.log);
  }

}
