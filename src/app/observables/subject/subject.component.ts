import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubjectService } from './subject-service';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.sass']
})
export class SubjectComponent implements OnInit, OnDestroy {

  nameControl: FormControl;

  subscription: Subscription;

  constructor(private subjectService: SubjectService) { }

  ngOnInit() {
    this.nameControl = new FormControl(this.subjectService.nameSubject.value);
    this.subscription = this.subjectService.nameSubject.subscribe(
        name => console.log('Name is ' + name));
    this.nameControl.valueChanges.subscribe(name => this.subjectService.updateName(name));
  }

  onClick() {
    this.subjectService.click();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
