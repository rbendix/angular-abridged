import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class SubjectService {
  clickSubject: Subject<void> = new Subject<void>();

  nameSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');

  click() {
    this.clickSubject.next();
  }

  updateName(name: string) {
    this.nameSubject.next(name);
  }
}
