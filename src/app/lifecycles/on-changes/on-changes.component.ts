import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-on-changes',
  templateUrl: './on-changes.component.html',
  styleUrls: ['./on-changes.component.sass']
})
export class OnChangesComponent implements OnChanges {

  @Input() title: string;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('Changes');
    console.log(changes);
  }

}
