import {
  AfterContentChecked,
  AfterContentInit, AfterViewChecked,
  AfterViewInit,
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-content-and-view-init-and-checked',
  templateUrl: './content-and-view-init-and-checked.component.html',
  styleUrls: ['./content-and-view-init-and-checked.component.sass']
})
export class ContentAndViewInitAndCheckedComponent implements AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked {

  constructor() { }

  ngAfterContentInit(): void {
    console.log('Content initialised');
  }

  ngAfterContentChecked(): void {
    console.log('Content checked');
  }

  ngAfterViewInit(): void {
    console.log('view initialised');
  }

  ngAfterViewChecked(): void {
    console.log('view checked');
  }

}
