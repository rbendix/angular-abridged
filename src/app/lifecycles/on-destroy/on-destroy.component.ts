import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-on-destroy',
  templateUrl: './on-destroy.component.html',
  styleUrls: ['./on-destroy.component.sass']
})
export class OnDestroyComponent implements OnDestroy {

  constructor() { }

  ngOnDestroy(): void {
    console.log('Destroyed');
  }

}
