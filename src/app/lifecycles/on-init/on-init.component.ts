import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-on-init',
  templateUrl: './on-init.component.html',
  styleUrls: ['./on-init.component.sass']
})
export class OnInitComponent implements OnInit {

  constructor() {
    console.log('Constructor');
  }

  ngOnInit() {
    console.log('OnInit');
  }

}
