import { Component, DoCheck, OnInit } from '@angular/core';

@Component({
  selector: 'app-do-check',
  templateUrl: './do-check.component.html',
  styleUrls: ['./do-check.component.sass']
})
export class DoCheckComponent implements OnInit, DoCheck {

  title = 'My Title';

  constructor() { }

  ngDoCheck(): void {
    console.log('Do check');
  }

  ngOnInit(): void {
    setTimeout(() => this.title = 'Change Title', 5000);
  }

  log() {
    console.log('Button clicked');
  }

}
