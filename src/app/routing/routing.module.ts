import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserResolver } from './user.resolver';
import { SampleGuard } from './sample.guard';
import { SampleChildGuard } from './sample-child.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: UserListComponent,
      },
      {
        path: ':id',
        data: { someProperty: 'SomeValue' },
        resolve: {
          user: UserResolver
        },
        component: UserDetailComponent
      }
    ]
  }
];

@NgModule({
  declarations: [UserListComponent, UserDetailComponent, UsersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  providers: [UserResolver]
})
export class RoutingModule { }
