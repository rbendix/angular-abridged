import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from './user';
import { delay } from 'rxjs/operators';

const users: User[] = [
  {
    id: '1',
    firstName: 'Max',
    lastName: 'Mustermann'
  },
  {
    id: '2',
    firstName: 'Gustav',
    lastName: 'Gans'
  },
  {
    id: '3',
    firstName: 'Marta',
    lastName: 'Musterfrau'
  }
];

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getAll(): Observable<User[]> {
    return of(users).pipe(delay(750));
  }

  getById(userId: string): Observable<User> {
    return of(users.find(user => user.id === userId)).pipe(delay(750));
  }
}
