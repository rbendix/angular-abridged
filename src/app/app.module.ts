import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DataBindingComponent } from './components/data-binding/data-binding.component';
import { FormsModule } from '@angular/forms';
import { DirectivesComponent } from './components/directives/directives.component';
import { TemplateDirectiveComponent } from './components/directives/template-directive/template-directive.component';
import { BlueTextDirective } from './components/directives/blue-text.directive';
import { ViewEncapsulationComponent } from './components/view-encapsulation/view-encapsulation.component';
import { CustomParagraphComponent } from './components/view-encapsulation/custom-paragraph/custom-paragraph.component';
import { LocalReferenceComponent } from './components/local-reference/local-reference.component';
import { ContentComponent } from './components/content/content.component';
import { OnChangesComponent } from './lifecycles/on-changes/on-changes.component';
import { OnInitComponent } from './lifecycles/on-init/on-init.component';
import { DoCheckComponent } from './lifecycles/do-check/do-check.component';
import {
  ContentAndViewInitAndCheckedComponent
} from './lifecycles/content-and-view-init-and-checked/content-and-view-init-and-checked.component';
import { OnDestroyComponent } from './lifecycles/on-destroy/on-destroy.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from './shared/shared.module';

const routes: Routes = [
  {
    path: 'di',
    loadChildren: './di/di.module#DiModule'
  },
  {
    path: 'di2',
    loadChildren: './di2/di2.module#Di2Module'
  },
  {
    path: 'users',
    loadChildren: './routing/routing.module#RoutingModule'
  },
  {
    path: 'forms',
    loadChildren: './forms-showcase/forms-showcase.module#FormsShowcaseModule'
  },
  {
    path: 'observables',
    loadChildren: './observables/observables.module#ObservablesModule'
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DataBindingComponent,
    DirectivesComponent,
    TemplateDirectiveComponent,
    BlueTextDirective,
    ViewEncapsulationComponent,
    CustomParagraphComponent,
    LocalReferenceComponent,
    ContentComponent,
    OnChangesComponent,
    OnInitComponent,
    DoCheckComponent,
    ContentAndViewInitAndCheckedComponent,
    OnDestroyComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
