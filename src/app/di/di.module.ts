import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiComponent } from './di/di.component';
import { MY_INJECTION_TOKEN } from '../shared/injection.tokens';
import { LoggingService } from '../shared/logging.service';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: '', component: DiComponent }
];

@NgModule({
  declarations: [DiComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [
    { provide: MY_INJECTION_TOKEN, useValue: { defaultMessage: 'DI Module Message'} },
    LoggingService
  ]
})
export class DiModule { }
