import { Component, OnInit } from '@angular/core';
import { LoggingService } from '../../shared/logging.service';
import { AnotherService } from '../another.service';

@Component({
  selector: 'app-di',
  templateUrl: './di.component.html',
  styleUrls: ['./di.component.sass']
})
export class DiComponent implements OnInit {

  constructor(private loggingService: LoggingService, private anotherService: AnotherService) { }

  ngOnInit() {
    this.anotherService.doSomething();
  }

  log() {
    this.loggingService.log('DI Component!');
  }

}
