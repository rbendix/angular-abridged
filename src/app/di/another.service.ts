import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AnotherService {
  doSomething() {
    console.log('Another service does something');
  }
}
