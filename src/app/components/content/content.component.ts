import { AfterContentInit, Component, ContentChild, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.sass']
})
export class ContentComponent implements OnInit, AfterContentInit {

  @ContentChild('myChild') myChild: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    console.log('Content of myChild: ' + this.myChild.nativeElement.textContent);
  }

}
