import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBlueText]'
})
export class BlueTextDirective {

  constructor(private elRef: ElementRef, renderer: Renderer2) {
    renderer.setStyle(elRef.nativeElement, 'color', '#316eee');
  }

}
