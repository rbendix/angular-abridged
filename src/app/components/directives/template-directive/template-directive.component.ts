import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-directive',
  templateUrl: './template-directive.component.html',
  styleUrls: ['./template-directive.component.sass']
})
export class TemplateDirectiveComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
