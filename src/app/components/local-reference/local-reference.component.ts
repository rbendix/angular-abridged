import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-local-reference',
  templateUrl: './local-reference.component.html',
  styleUrls: ['./local-reference.component.sass']
})
export class LocalReferenceComponent implements OnInit {

  @ViewChild('myInput') myInput: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  log() {
    console.log(this.myInput.nativeElement.value);
  }



}
