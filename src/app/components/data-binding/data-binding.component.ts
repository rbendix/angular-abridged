import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.sass']
})
export class DataBindingComponent implements OnInit {

  name = 'Some Name';
  oneWayProp = 'I cannot change this';
  active = false;
  twoWayProp = 'This I can change';

  constructor() { }

  ngOnInit() {
  }

  activate() {
    this.active = true;
  }

}
