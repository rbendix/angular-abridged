import { Component, Inject, OnInit } from '@angular/core';
import { LoggingService } from '../../shared/logging.service';
import { MY_INJECTION_TOKEN } from '../../shared/injection.tokens';
import { ServiceConfig } from '../../shared/injection-token.types';

@Component({
  selector: 'app-injection',
  templateUrl: './injection.component.html',
  styleUrls: ['./injection.component.sass']
})
export class InjectionComponent implements OnInit {

  constructor(private loggingService: LoggingService, @Inject(MY_INJECTION_TOKEN) private config: ServiceConfig) { }

  ngOnInit() {
    console.log('Component init: ' + this.config.defaultMessage);
  }

  log() {
    this.loggingService.log('Some message');
  }

}
