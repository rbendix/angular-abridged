import { NgModule } from '@angular/core';
import { InjectionComponent } from './injection/injection.component';
import { LoggingService } from '../shared/logging.service';

@NgModule({
  declarations: [
      InjectionComponent
  ],
  providers: [
    { provide: LoggingService, useClass: LoggingService }
  ],
  exports: [
      InjectionComponent
  ]
})
export class DependencyInjectionModule {}
