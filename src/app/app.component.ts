import { Component, OnInit } from '@angular/core';
import { SubjectService } from './observables/subject/subject-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'angular-abridged';
  show = true;

  constructor(private subjectService: SubjectService) {}

  changeTitle() {
    this.title = 'Different title';
  }

  ngOnInit(): void {
    this.subjectService.clickSubject.subscribe(() => console.log('AppComponent Clicked!'));
  }
}
